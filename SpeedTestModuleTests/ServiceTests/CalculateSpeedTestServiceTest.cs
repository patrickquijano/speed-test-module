﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpeedTestModule.IServices;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SpeedTestModuleTests.ServiceTests
{
    [TestClass]
    public class CalculateSpeedTestServiceTest : TestBase
    {
        private ICalculateSpeedTestService _CalculateSpeedTestService;

        [TestInitialize]
        public void Setup()
        {
            this._CalculateSpeedTestService = this.Resolve<ICalculateSpeedTestService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public async Task CalculateSpeedTestAsync()
        {
            var speedTest = await this._CalculateSpeedTestService.CalculateAsync(m => Debug.WriteLine(m));

            Assert.IsNotNull(speedTest);
        }
    }
}
