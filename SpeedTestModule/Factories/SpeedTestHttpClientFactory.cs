﻿using SpeedTestModule.Extensions;
using SpeedTestModule.IFactories;
using System;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;

namespace SpeedTestModule.Factories
{
    internal class SpeedTestHttpClientFactory : ISpeedTestHttpClientFactory
    {
        private readonly Version _AssemblyVersion;

        public SpeedTestHttpClientFactory(Version assemblyVersion)
        {
            this._AssemblyVersion = assemblyVersion;
        }

        public HttpClient Create()
        {
            var client = new HttpClient();
            var operatingSystemDescriptionArray = RuntimeInformation.OSDescription.Split();
            var operatingSystemDescription1 = operatingSystemDescriptionArray.FirstOrDefault();
            var operatingSystemDescription2 = operatingSystemDescriptionArray.Skip(1).FirstOrDefault();
            var frameworkDescriptionArray = RuntimeInformation.FrameworkDescription.Split();
            var frameworkDescription1 = frameworkDescriptionArray.Skip(frameworkDescriptionArray.Length - 2).FirstOrDefault();
            var frameworkDescription2 = frameworkDescriptionArray.Skip(frameworkDescriptionArray.Length - 1).FirstOrDefault();
            client.DefaultRequestHeaders.Add("Accept", "text/html, application/xhtml+xml, */*");
            var userAgent = new string[] {
                "Mozilla/5.0",
                $"({operatingSystemDescription1}-{operatingSystemDescription2}; U; {RuntimeInformation.ProcessArchitecture}; en-us)",
                $"{frameworkDescription1}/{frameworkDescription2}",
                "(KHTML, like Gecko)",
                $"SpeedTest.Net/{this._AssemblyVersion}",
            };
            client.DefaultRequestHeaders.Add("User-Agent", userAgent.StringJoin(" "));

            return client;
        }
    }
}
