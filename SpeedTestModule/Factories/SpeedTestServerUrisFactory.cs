﻿using SpeedTestModule.Extensions;
using SpeedTestModule.IFactories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SpeedTestModule.Factories
{
    internal class SpeedTestServerUrisFactory : ISpeedTestServerUrisFactory
    {
        private readonly IEnumerable<string> _SpeedTestServerUrls;

        public SpeedTestServerUrisFactory(IEnumerable<string> speedTestServerUrls)
        {
            this._SpeedTestServerUrls = speedTestServerUrls;
        }

        public IEnumerable<Uri> Create()
        {
            return this._SpeedTestServerUrls.Select(url => new Uri(url).AddQuery("x", DateTime.Now.ToFileTime().ToString(CultureInfo.InvariantCulture)));
        }
    }
}
