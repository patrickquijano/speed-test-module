﻿using SpeedTestModule.IFactories;
using SpeedTestModule.Models.SpeedTestConfig;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpeedTestModule.Factories
{
    internal class UploadStringsFactory : IUploadStringsFactory
    {
        private readonly int _RetryCount;

        public UploadStringsFactory(int retryCount)
        {
            this._RetryCount = retryCount;
        }

        public IEnumerable<string> Create(Upload upload)
        {
            if (upload == null)
            {
                throw new ArgumentNullException($"The argument `{nameof(upload)}` of type {upload.GetType()} is null.");
            }
            var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var random = new Random();
            var uploadStrings = new List<string>();
            for (var sizeCounter = 1; sizeCounter < upload.ThreadsPerUrl + 1; sizeCounter++)
            {
                var size = sizeCounter * 200 * 1024;
                var builder = new StringBuilder(size);
                builder.Append($"content{sizeCounter}");
                for (var i = 0; i < size; ++i)
                {
                    builder.Append(characters[random.Next(characters.Length)]);
                }
                for (var i = 0; i < this._RetryCount; i++)
                {
                    uploadStrings.Add(builder.ToString());
                }
            }

            return uploadStrings;
        }
    }
}
