﻿using SpeedTestModule.Extensions;
using SpeedTestModule.IFactories;
using System;
using System.Globalization;

namespace SpeedTestModule.Factories
{
    internal class SpeedTestConfigUriFactory : ISpeedTestConfigUriFactory
    {
        private readonly string _SpeedTestConfigUrl;

        public SpeedTestConfigUriFactory(string speedTestConfigUrl)
        {
            this._SpeedTestConfigUrl = speedTestConfigUrl;
        }

        public Uri Create()
        {
            return new Uri(this._SpeedTestConfigUrl).AddQuery("x", DateTime.Now.ToFileTime().ToString(CultureInfo.InvariantCulture));
        }
    }
}
