﻿using SpeedTestModule.Extensions;
using SpeedTestModule.IFactories;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;

namespace SpeedTestModule.Factories
{
    internal class DownloadUrisFactory : IDownloadUrisFactory
    {
        private readonly int _RetryCount;
        private readonly IUriFactory _UriFactory;

        public DownloadUrisFactory(int retryCount,
            IUriFactory uriFactory)
        {
            this._RetryCount = retryCount;
            this._UriFactory = uriFactory;
        }

        public IEnumerable<Uri> Create(Server server)
        {
            if (server == null)
            {
                throw new ArgumentNullException($"The argument `{nameof(server)}` of type {server.GetType()} is null.");
            }
            var downloadSizes = new int[] { 350, 750, 1500, 3000 };
            var uris = new List<Uri>();
            foreach (var downloadSize in downloadSizes)
            {
                for (var i = 0; i < this._RetryCount; i++)
                {
                    uris.Add(this._UriFactory.Create(server.Url, $"random{downloadSize}x{downloadSize}.jpg").AddQuery("r", "1"));
                }
            }

            return uris;
        }
    }
}
