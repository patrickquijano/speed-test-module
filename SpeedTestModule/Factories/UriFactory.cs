﻿using SpeedTestModule.IFactories;
using System;

namespace SpeedTestModule.Factories
{
    internal class UriFactory : IUriFactory
    {
        public Uri Create(string url, params string[] paths)
        {
            var uri = new Uri(url);
            if (paths != null)
            {
                foreach (var path in paths)
                {
                    uri = new Uri(uri, path);
                }
            }

            return uri;
        }
    }
}
