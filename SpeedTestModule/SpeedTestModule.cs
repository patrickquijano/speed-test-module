﻿using Autofac;
using SpeedTestModule.Factories;
using SpeedTestModule.IFactories;
using SpeedTestModule.IServices;
using SpeedTestModule.Services;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace SpeedTestModule
{
    public class SpeedTestModule : Autofac.Module
    {
        public Assembly Assembly { get; set; }
        public Version AssemblyVersion { get; set; }
        public int RetryCount { get; set; }
        public int ServerDistanceInKilometersRadiusLimit { get; set; }
        public int ServerLatencyLimit { get; set; }
        public string SpeedTestConfigUrl { get; set; }
        public IEnumerable<string> SpeedTestServerUrls { get; set; }

        public SpeedTestModule()
        {
            this.Assembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
            this.AssemblyVersion = this.Assembly.GetName().Version;
            this.RetryCount = 3;
            this.ServerDistanceInKilometersRadiusLimit = 500;
            this.ServerLatencyLimit = 200;
            this.SpeedTestConfigUrl = @"https://www.speedtest.net/speedtest-config.php";
            this.SpeedTestServerUrls = new List<string>
            {
                @"https://www.speedtest.net/speedtest-servers-static.php",
                @"https://c.speedtest.net/speedtest-servers-static.php",
                @"https://www.speedtest.net/speedtest-servers.php",
                @"https://c.speedtest.net/speedtest-servers.php",
            };
        }

        protected override void Load(ContainerBuilder builder)
        {
            this.RegisterFactories(builder);
            this.RegisterServices(builder);
        }

        private void RegisterFactories(ContainerBuilder builder)
        {
            builder.RegisterType<DownloadUrisFactory>().As<IDownloadUrisFactory>().SingleInstance()
                .WithParameter("retryCount", this.RetryCount);
            builder.RegisterType<SpeedTestConfigUriFactory>().As<ISpeedTestConfigUriFactory>().SingleInstance()
                .WithParameter("speedTestConfigUrl", this.SpeedTestConfigUrl);
            builder.RegisterType<SpeedTestHttpClientFactory>().As<ISpeedTestHttpClientFactory>().SingleInstance()
                .WithParameter("assemblyVersion", this.AssemblyVersion);
            builder.RegisterType<SpeedTestServerUrisFactory>().As<ISpeedTestServerUrisFactory>().SingleInstance()
                .WithParameter("speedTestServerUrls", this.SpeedTestServerUrls);
            builder.RegisterType<UploadStringsFactory>().As<IUploadStringsFactory>().SingleInstance()
                .WithParameter("retryCount", this.RetryCount);
            builder.RegisterType<UriFactory>().As<IUriFactory>().SingleInstance();
        }

        private void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<CalculateDownloadSpeedService>().As<ICalculateDownloadSpeedService>().SingleInstance();
            builder.RegisterType<CalculateSpeedTestServerDistanceService>().As<ICalculateSpeedTestServerDistanceService>().SingleInstance()
                .WithParameter("speedTestServerDistanceInKilometersRadiusLimit", this.ServerDistanceInKilometersRadiusLimit);
            builder.RegisterType<CalculateSpeedTestServerLatencyService>().As<ICalculateSpeedTestServerLatencyService>().SingleInstance()
                .WithParameter("speedTestServerLatencyLimit", this.ServerLatencyLimit);
            builder.RegisterType<CalculateSpeedTestService>().As<ICalculateSpeedTestService>().SingleInstance();
            builder.RegisterType<CalculateUploadSpeedService>().As<ICalculateUploadSpeedService>().SingleInstance();
            builder.RegisterType<GetSpeedTestServersService>().As<IGetSpeedTestServersService>().SingleInstance();
            builder.RegisterType<GetSpeedTestSettingsService>().As<IGetSpeedTestSettingsService>().SingleInstance();
        }
    }
}
