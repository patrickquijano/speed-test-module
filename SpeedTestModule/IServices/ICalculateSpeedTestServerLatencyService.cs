﻿using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpeedTestModule.IServices
{
    public interface ICalculateSpeedTestServerLatencyService
    {
        Task<IEnumerable<Server>> CalculateAsync(IEnumerable<Server> servers);
        Task<IEnumerable<Server>> CalculateAsync(IEnumerable<Server> servers, Action<string> messageCallback);
    }
}