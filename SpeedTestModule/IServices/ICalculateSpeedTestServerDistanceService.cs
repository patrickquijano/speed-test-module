﻿using SpeedTestModule.Models.SpeedTestConfig;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpeedTestModule.IServices
{
    public interface ICalculateSpeedTestServerDistanceService
    {
        Task<IEnumerable<Server>> CalculateAsync(IEnumerable<Server> servers, Client client);
        Task<IEnumerable<Server>> CalculateAsync(IEnumerable<Server> servers, Client client, Action<string> messageCallback);
    }
}