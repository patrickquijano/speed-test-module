﻿using SpeedTestModule.Models.SpeedTestConfig;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Threading.Tasks;

namespace SpeedTestModule.IServices
{
    public interface ICalculateUploadSpeedService
    {
        Task<double> CalculateAsync(Upload upload, Server server);
        Task<double> CalculateAsync(Upload upload, Server server, Action<string> messageCallback);
    }
}