﻿using SpeedTestModule.Models.SpeedTestConfig;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Threading.Tasks;

namespace SpeedTestModule.IServices
{
    public interface ICalculateDownloadSpeedService
    {
        Task<double> CalculateAsync(Server server, Download download);
        Task<double> CalculateAsync(Server server, Download download, Action<string> messageCallback);
    }
}