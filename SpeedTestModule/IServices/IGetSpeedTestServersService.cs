﻿using SpeedTestModule.Models.SpeedTestConfig;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpeedTestModule.IServices
{
    public interface IGetSpeedTestServersService
    {
        Task<IEnumerable<Server>> GetAsync(ServerConfig serverConfig, Client client);
        Task<IEnumerable<Server>> GetAsync(ServerConfig serverConfig, Client client, Action<string> messageCallback);
    }
}