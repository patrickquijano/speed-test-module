﻿using SpeedTestModule.Models;
using System;
using System.Threading.Tasks;

namespace SpeedTestModule.IServices
{
    public interface ICalculateSpeedTestService
    {
        Task<SpeedTest> CalculateAsync();
        Task<SpeedTest> CalculateAsync(Action<string> messageCallback);
    }
}