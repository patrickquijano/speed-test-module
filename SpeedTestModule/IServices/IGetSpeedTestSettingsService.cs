﻿using SpeedTestModule.Models.SpeedTestConfig;
using System;
using System.Threading.Tasks;

namespace SpeedTestModule.IServices
{
    public interface IGetSpeedTestSettingsService
    {
        Task<Settings> GetAsync();
        Task<Settings> GetAsync(Action<string> messageCallback);
    }
}