﻿using SpeedTestModule.IFactories;
using SpeedTestModule.IServices;
using SpeedTestModule.Models.SpeedTestConfig;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SpeedTestModule.Services
{
    internal class CalculateUploadSpeedService : ICalculateUploadSpeedService
    {
        private readonly ISpeedTestHttpClientFactory _SpeedTestHttpClientFactory;
        private readonly IUploadStringsFactory _UploadStringsFactory;

        public CalculateUploadSpeedService(ISpeedTestHttpClientFactory speedTestHttpClientFactory,
            IUploadStringsFactory uploadStringsFactory)
        {
            this._SpeedTestHttpClientFactory = speedTestHttpClientFactory;
            this._UploadStringsFactory = uploadStringsFactory;
        }

        public Task<double> CalculateAsync(Upload upload, Server server)
        {
            return this.CalculateAsync(upload, server, null);
        }

        public async Task<double> CalculateAsync(Upload upload, Server server, Action<string> messageCallback)
        {
            if (upload == null)
            {
                throw new ArgumentNullException($"The argument `{nameof(upload)}` of type {upload.GetType()} is null.");
            }
            if (server == null)
            {
                throw new ArgumentNullException($"The argument `{nameof(server)}` of type {server.GetType()} is null.");
            }
            messageCallback?.Invoke("Calculating upload speed.");
            var uploadStrings = this._UploadStringsFactory.Create(upload);
            var tasks = new List<Task<int>>();
            var taskResults = new List<int>();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            using (var semaphoreSlim = new SemaphoreSlim(upload.ThreadsPerUrl))
            {
                foreach (var uploadString in uploadStrings)
                {
                    var task = Task.Run(async () =>
                    {
                        await semaphoreSlim.WaitAsync();
                        try
                        {
                            using (var speedTestHttpClient = this._SpeedTestHttpClientFactory.Create())
                            {
                                await speedTestHttpClient.PostAsync(server.Url, new StringContent(uploadString));

                                return uploadString.Length;
                            }
                        }
                        finally
                        {
                            semaphoreSlim.Release();
                        }
                    });
                    tasks.Add(task);
                }
                taskResults = (await Task.WhenAll(tasks)).ToList();
            }
            stopwatch.Stop();
            var uploadSpeed = taskResults.Sum() / (stopwatch.ElapsedMilliseconds / 1000.0D) * 8;
            messageCallback?.Invoke($"Upload Speed: {uploadSpeed / 1000000:n2} Mbps");

            return uploadSpeed;
        }
    }
}
