﻿using SpeedTestModule.IFactories;
using SpeedTestModule.IServices;
using SpeedTestModule.Models.SpeedTestConfig;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SpeedTestModule.Services
{
    internal class GetSpeedTestSettingsService : IGetSpeedTestSettingsService
    {
        private readonly ISpeedTestConfigUriFactory _SpeedTestConfigUriFactory;
        private readonly ISpeedTestHttpClientFactory _SpeedTestHttpClientFactory;

        public GetSpeedTestSettingsService(ISpeedTestConfigUriFactory speedTestConfigUriFactory,
            ISpeedTestHttpClientFactory speedTestHttpClientFactory)
        {
            this._SpeedTestConfigUriFactory = speedTestConfigUriFactory;
            this._SpeedTestHttpClientFactory = speedTestHttpClientFactory;
        }

        public Task<Settings> GetAsync()
        {
            return this.GetAsync(null);
        }

        public async Task<Settings> GetAsync(Action<string> messageCallback)
        {
            messageCallback?.Invoke("Retrieving Speed Test settings.");
            using (var httpClient = this._SpeedTestHttpClientFactory.Create())
            {
                var uri = this._SpeedTestConfigUriFactory.Create();
                var response = await httpClient.GetStringAsync(uri);
                var serializer = new XmlSerializer(typeof(Settings));
                using (var reader = new StringReader(response))
                {
                    messageCallback?.Invoke("Successfully retrieved Speed Test settings.");
                    return (Settings)serializer.Deserialize(reader);
                }
            }
        }
    }
}
