﻿using SpeedTestModule.IServices;
using SpeedTestModule.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SpeedTestModule.Services
{
    internal class CalculateSpeedTestService : ICalculateSpeedTestService
    {
        private readonly ICalculateDownloadSpeedService _CalculateDownloadSpeedService;
        private readonly ICalculateSpeedTestServerDistanceService _CalculateSpeedTestServerDistanceService;
        private readonly ICalculateSpeedTestServerLatencyService _CalculateSpeedTestServerLatencyService;
        private readonly ICalculateUploadSpeedService _CalculateUploadSpeedService;
        private readonly IGetSpeedTestServersService _GetSpeedTestServersService;
        private readonly IGetSpeedTestSettingsService _GetSpeedTestSettingsService;

        public CalculateSpeedTestService(ICalculateDownloadSpeedService calculateDownloadSpeedService,
            ICalculateSpeedTestServerDistanceService calculateSpeedTestServerDistanceService,
            ICalculateSpeedTestServerLatencyService calculateSpeedTestServerLatencyService,
            ICalculateUploadSpeedService calculateUploadSpeedService,
            IGetSpeedTestServersService getSpeedTestServersService,
            IGetSpeedTestSettingsService getSpeedTestSettingsService)
        {
            this._CalculateDownloadSpeedService = calculateDownloadSpeedService;
            this._CalculateSpeedTestServerDistanceService = calculateSpeedTestServerDistanceService;
            this._CalculateSpeedTestServerLatencyService = calculateSpeedTestServerLatencyService;
            this._CalculateUploadSpeedService = calculateUploadSpeedService;
            this._GetSpeedTestServersService = getSpeedTestServersService;
            this._GetSpeedTestSettingsService = getSpeedTestSettingsService;
        }

        public Task<SpeedTest> CalculateAsync()
        {
            return this.CalculateAsync(null);
        }

        public async Task<SpeedTest> CalculateAsync(Action<string> messageCallback)
        {
            messageCallback?.Invoke("Calculating Speed Test.");
            var settings = await this._GetSpeedTestSettingsService.GetAsync(messageCallback);
            var servers = await this._GetSpeedTestServersService.GetAsync(settings.ServerConfig, settings.Client, messageCallback);
            var server = servers.OrderBy(s => s.Latency).FirstOrDefault();
            messageCallback?.Invoke($"Best Server: Hosted by {server.Sponsor} ({server.Name}/{server.Country}), distance: {server.DistanceInKilometers:n0}km, latency: {server.Latency:n0}ms");
            var downloadSpeed = await this._CalculateDownloadSpeedService.CalculateAsync(server, settings.Download, messageCallback);
            var uploadSpeed = await this._CalculateUploadSpeedService.CalculateAsync(settings.Upload, server, messageCallback);
            var speedTest = new SpeedTest
            {
                DownloadSpeed = downloadSpeed,
                Server = server,
                UploadSpeed = uploadSpeed,
            };
            messageCallback?.Invoke("Successfully calculated Speed Test.");

            return speedTest;
        }
    }
}
