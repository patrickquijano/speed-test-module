﻿using SpeedTestModule.Extensions;
using SpeedTestModule.IFactories;
using SpeedTestModule.IServices;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SpeedTestModule.Services
{
    internal class GetSpeedTestServersService : IGetSpeedTestServersService
    {
        private readonly ISpeedTestHttpClientFactory _SpeedTestHttpClientFactory;
        private readonly ISpeedTestServerUrisFactory _SpeedTestServerUrisFactory;
        private readonly ICalculateSpeedTestServerDistanceService _CalculateSpeedTestServerDistanceService;
        private readonly ICalculateSpeedTestServerLatencyService _CalculateSpeedTestServerLatencyService;

        public GetSpeedTestServersService(ISpeedTestHttpClientFactory speedTestHttpClientFactory,
            ISpeedTestServerUrisFactory speedTestServerUrisFactory,
            ICalculateSpeedTestServerDistanceService calculateSpeedTestServerDistanceService,
            ICalculateSpeedTestServerLatencyService calculateSpeedTestServerLatencyService)
        {
            this._SpeedTestHttpClientFactory = speedTestHttpClientFactory;
            this._SpeedTestServerUrisFactory = speedTestServerUrisFactory;
            this._CalculateSpeedTestServerDistanceService = calculateSpeedTestServerDistanceService;
            this._CalculateSpeedTestServerLatencyService = calculateSpeedTestServerLatencyService;
        }

        public Task<IEnumerable<Server>> GetAsync(Models.SpeedTestConfig.ServerConfig serverConfig, Models.SpeedTestConfig.Client client)
        {
            return this.GetAsync(serverConfig, client, null);
        }

        public async Task<IEnumerable<Server>> GetAsync(Models.SpeedTestConfig.ServerConfig serverConfig, Models.SpeedTestConfig.Client client, Action<string> messageCallback)
        {
            if (serverConfig == null)
            {
                throw new ArgumentNullException($"The argument `{nameof(serverConfig)}` of type {serverConfig.GetType()} is null.");
            }
            messageCallback?.Invoke("Retrieving Speed Test servers.");
            var tasks = new List<Task<Settings>>();
            var speedTestUris = this._SpeedTestServerUrisFactory.Create();
            foreach (var speedTestUri in speedTestUris)
            {
                var task = Task.Run(async () =>
                {
                    using (var speedTestHttpClient = this._SpeedTestHttpClientFactory.Create())
                    {
                        var response = await speedTestHttpClient.GetStringAsync(speedTestUri);
                        var serializer = new XmlSerializer(typeof(Settings));
                        using (var reader = new StringReader(response))
                        {
                            return (Settings)serializer.Deserialize(reader);
                        }
                    }
                });
                tasks.Add(task);
            }
            var settings = await Task.WhenAll(tasks);
            var servers = settings.SelectMany(setting => setting.Servers.Items)
                .Distinct();
            if (serverConfig != null && !serverConfig.IgnoreIds.IsNullOrWhiteSpace())
            {
                servers = servers.Where(server => !serverConfig.IgnoreIds.Split(',').Contains(server.Id.ToString()));
            }
            servers = await this._CalculateSpeedTestServerDistanceService.CalculateAsync(servers, client, messageCallback);
            servers = await this._CalculateSpeedTestServerLatencyService.CalculateAsync(servers, messageCallback);
            messageCallback?.Invoke("Successfully retrieved Speed Test servers.");

            return servers;
        }
    }
}
