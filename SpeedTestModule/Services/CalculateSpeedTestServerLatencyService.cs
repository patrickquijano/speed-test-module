﻿using SpeedTestModule.Extensions;
using SpeedTestModule.IFactories;
using SpeedTestModule.IServices;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SpeedTestModule.Services
{
    internal class CalculateSpeedTestServerLatencyService : ICalculateSpeedTestServerLatencyService
    {
        private readonly int _SpeedTestServerLatencyLimit;
        private readonly ISpeedTestHttpClientFactory _SpeedTestHttpClientFactory;
        private readonly IUriFactory _UriFactory;

        public CalculateSpeedTestServerLatencyService(int speedTestServerLatencyLimit,
            ISpeedTestHttpClientFactory speedTestHttpClientFactory,
            IUriFactory uriFactory)
        {
            this._SpeedTestServerLatencyLimit = speedTestServerLatencyLimit;
            this._SpeedTestHttpClientFactory = speedTestHttpClientFactory;
            this._UriFactory = uriFactory;
        }

        public Task<IEnumerable<Server>> CalculateAsync(IEnumerable<Server> servers)
        {
            return this.CalculateAsync(servers, null);
        }

        public async Task<IEnumerable<Server>> CalculateAsync(IEnumerable<Server> servers, Action<string> messageCallback)
        {
            if (servers == null || !servers.Any())
            {
                throw new ArgumentNullException($"The argument `{nameof(servers)}` of type {servers.GetType()} is either null or does not contain any items.");
            }
            messageCallback?.Invoke("Calculating Speed Test server latency.");
            var tasks = new List<Task>();
            foreach (var server in servers)
            {
                var task = Task.Run(async () =>
                {
                    var timer = new Stopwatch();
                    var retrySuccessCount = 0;
                    server.Latency = -1;
                    for (var i = 0; i < 4; i++)
                    {
                        try
                        {
                            timer.Start();
                            using (var speedTestHttpClient = this._SpeedTestHttpClientFactory.Create())
                            {
                                speedTestHttpClient.Timeout = TimeSpan.FromMilliseconds(this._SpeedTestServerLatencyLimit);
                                var uri = this._UriFactory.Create(server.Url, "latency.txt");
                                var response = await speedTestHttpClient.GetStringAsync(uri);
                                if (!response.StartsWith("test=test"))
                                {
                                    timer.Stop();
                                    break;
                                }
                            }
                            retrySuccessCount++;
                        }
                        catch
                        {
                            continue;
                        }
                        finally
                        {
                            timer.Stop();
                        }
                    }
                    if (retrySuccessCount > 0)
                    {
                        server.Latency = (double)timer.ElapsedMilliseconds / retrySuccessCount;
                    }
                });
                tasks.Add(task);
            }
            await Task.WhenAll(tasks);
            messageCallback?.Invoke("Successfully calculated Speed Test server latency.");
            servers = servers.Where(server => server.Latency > -1 && server.Latency < this._SpeedTestServerLatencyLimit)
                .OrderBy(server => server.Latency);
            messageCallback?.Invoke($"Servers filtered with {this._SpeedTestServerLatencyLimit}ms or less latency. {servers.Count()} server{(servers.Count() > 1 ? "s" : "")} available.");
            servers.ForEach((server, index) => messageCallback?.Invoke($"{index + 1}. Hosted by {server.Sponsor} ({server.Name}/{server.Country}), latency: {server.Latency:n0}ms"));

            return servers;
        }
    }
}
