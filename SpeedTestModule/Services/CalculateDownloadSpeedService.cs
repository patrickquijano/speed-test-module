﻿using SpeedTestModule.IFactories;
using SpeedTestModule.IServices;
using SpeedTestModule.Models.SpeedTestConfig;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SpeedTestModule.Services
{
    internal class CalculateDownloadSpeedService : ICalculateDownloadSpeedService
    {
        private readonly IDownloadUrisFactory _DownloadUrisFactory;
        private readonly ISpeedTestHttpClientFactory _SpeedTestHttpClientFactory;

        public CalculateDownloadSpeedService(IDownloadUrisFactory downloadUrisFactory,
            ISpeedTestHttpClientFactory speedTestHttpClientFactory)
        {
            this._DownloadUrisFactory = downloadUrisFactory;
            this._SpeedTestHttpClientFactory = speedTestHttpClientFactory;
        }

        public Task<double> CalculateAsync(Server server, Download download)
        {
            return this.CalculateAsync(server, download, null);
        }

        public async Task<double> CalculateAsync(Server server, Download download, Action<string> messageCallback)
        {
            if (server == null)
            {
                throw new ArgumentNullException($"The argument `{nameof(server)}` of type {server.GetType()} is null.");
            }
            if (download == null)
            {
                throw new ArgumentNullException($"The argument `{nameof(download)}` of type {download.GetType()} is null.");
            }
            messageCallback?.Invoke("Calculating download speed.");
            var uris = this._DownloadUrisFactory.Create(server);
            var tasks = new List<Task<int>>();
            var taskResults = new List<int>();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            using (var semaphoreSlim = new SemaphoreSlim(download.ThreadsPerUrl))
            {
                foreach (var uri in uris)
                {
                    var task = Task.Run(async () =>
                    {
                        await semaphoreSlim.WaitAsync();
                        try
                        {
                            using (var speedTestHttpClient = this._SpeedTestHttpClientFactory.Create())
                            {
                                var data = await speedTestHttpClient.GetByteArrayAsync(uri);

                                return data.Length;
                            }
                        }
                        finally
                        {
                            semaphoreSlim.Release();
                        }
                    });
                    tasks.Add(task);
                }
                taskResults = (await Task.WhenAll(tasks)).ToList();
            }
            stopwatch.Stop();
            var downloadSpeed = taskResults.Sum() / (stopwatch.ElapsedMilliseconds / 1000.0D) * 8;
            messageCallback?.Invoke($"Download Speed: {downloadSpeed / 1000000:n2} Mbps");

            return downloadSpeed;
        }
    }
}
