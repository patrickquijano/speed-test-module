﻿using SpeedTestModule.Extensions;
using SpeedTestModule.IServices;
using SpeedTestModule.Models.SpeedTestConfig;
using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpeedTestModule.Services
{
    internal class CalculateSpeedTestServerDistanceService : ICalculateSpeedTestServerDistanceService
    {
        private readonly int _SpeedTestServerDistanceInKilometersRadiusLimit;

        public CalculateSpeedTestServerDistanceService(int speedTestServerDistanceInKilometersRadiusLimit)
        {
            this._SpeedTestServerDistanceInKilometersRadiusLimit = speedTestServerDistanceInKilometersRadiusLimit;
        }

        public Task<IEnumerable<Server>> CalculateAsync(IEnumerable<Server> servers, Client client)
        {
            return this.CalculateAsync(servers, client, null);
        }

        public async Task<IEnumerable<Server>> CalculateAsync(IEnumerable<Server> servers, Client client, Action<string> messageCallback)
        {
            if (servers == null || !servers.Any())
            {
                throw new ArgumentNullException($"The argument `{nameof(servers)}` of type {servers.GetType()} is either null or does not contain any items.");
            }
            if (client == null)
            {
                throw new ArgumentNullException($"The argument `{nameof(client)}` of type {client.GetType()} is null.");
            }
            messageCallback?.Invoke("Calculating Speed Test server distance.");
            await Task.WhenAll(servers.Select(server => Task.Run(() => server.DistanceInMeters = server.Location.CalculateDistance(client.Location))));
            messageCallback?.Invoke("Successfully calculated Speed Test server distance.");
            servers = servers.Where(server => server.DistanceInKilometers < this._SpeedTestServerDistanceInKilometersRadiusLimit)
                .OrderBy(server => server.DistanceInKilometers);
            messageCallback?.Invoke($"Servers filtered within {this._SpeedTestServerDistanceInKilometersRadiusLimit}km radius. {servers.Count()} server{(servers.Count() > 1 ? "s" : "")} available.");
            servers.ForEach((server, index) => messageCallback?.Invoke($"{index + 1}. Hosted by {server.Sponsor} ({server.Name}/{server.Country}), distance: {server.DistanceInKilometers:n0}km"));

            return servers;
        }
    }
}
