﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestServers
{
    [XmlRoot("settings")]
    public class Settings
    {
        [XmlElement("servers")]
        public Servers Servers { get; set; }
    }
}
