﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestServers
{
    [XmlRoot("servers")]
    public class Servers
    {
        [XmlElement("server")]
        public Server[] Items { get; set; }
    }
}
