﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestServers
{
    [XmlRoot("server")]
    public class Server
    {
        [XmlAttribute("url")]
        public string Url { get; set; }
        [XmlAttribute("lat")]
        public double Latitude { get; set; }
        [XmlAttribute("lon")]
        public double Longitude { get; set; }
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("country")]
        public string Country { get; set; }
        [XmlAttribute("cc")]
        public string CountryCode { get; set; }
        [XmlAttribute("sponsor")]
        public string Sponsor { get; set; }
        [XmlAttribute("id")]
        public string Id { get; set; }
        [XmlAttribute("host")]
        public string Host { get; set; }
        public GeoLocation Location => new GeoLocation
        {
            Latitude = this.Latitude,
            Longitude = this.Longitude,
        };
        public double DistanceInMeters { get; set; }
        public double DistanceInKilometers => this.DistanceInMeters / 1000;
        public double Latency { get; set; }
    }
}
