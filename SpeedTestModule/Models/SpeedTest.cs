﻿using SpeedTestModule.Models.SpeedTestServers;
using System.Text;

namespace SpeedTestModule.Models
{
    public class SpeedTest
    {
        public Server Server { get; set; }
        public double DownloadSpeed { get; set; }
        public double DownloadSpeedKbps => this.DownloadSpeed / 1000;
        public double DownloadSpeedMbps => this.DownloadSpeedKbps / 1000;
        public double UploadSpeed { get; set; }
        public double UploadSpeedKbps => this.UploadSpeed / 1000;
        public double UploadSpeedMbps => this.UploadSpeedKbps / 1000;

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendLine($"Best Server: Hosted by {this.Server.Sponsor} ({this.Server.Name}/{this.Server.Country}), distance: {this.Server.DistanceInKilometers:n0}km, latency: {this.Server.Latency:n0}ms");
            builder.AppendLine($"Download Speed: {this.DownloadSpeedMbps:n2} Mbps");
            builder.AppendLine($"Upload Speed: {this.UploadSpeedMbps:n2} Mbps");

            return builder.ToString();
        }
    }
}
