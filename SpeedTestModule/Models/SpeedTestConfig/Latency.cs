﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("latency")]
    public class Latency
    {
        [XmlAttribute("testlength")]
        public long TestLength { get; set; }
        [XmlAttribute("waittime")]
        public long WaitTime { get; set; }
        [XmlAttribute("timeout")]
        public long Timeout { get; set; }
    }
}
