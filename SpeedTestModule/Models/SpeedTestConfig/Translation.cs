﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("translation")]
    public class Translation
    {
        [XmlAttribute("lang")]
        public string Language { get; set; }
    }
}
