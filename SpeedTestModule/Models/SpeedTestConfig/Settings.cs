﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("settings")]
    public class Settings
    {
        [XmlElement("client")]
        public Client Client { get; set; }
        [XmlElement("server-config")]
        public ServerConfig ServerConfig { get; set; }
        [XmlElement("licensekey")]
        public string LicenseKey { get; set; }
        [XmlElement("customer")]
        public string Customer { get; set; }
        [XmlElement("odometer")]
        public Odometer Odometer { get; set; }
        [XmlElement("times")]
        public Times Times { get; set; }
        [XmlElement("download")]
        public Download Download { get; set; }
        [XmlElement("upload")]
        public Upload Upload { get; set; }
        [XmlElement("latency")]
        public Latency Latency { get; set; }
        [XmlElement("socket-download")]
        public SocketDownload SocketDownload { get; set; }
        [XmlElement("socket-upload")]
        public SocketUpload SocketUpload { get; set; }
        [XmlElement("socket-latency")]
        public SocketLatency SocketLatency { get; set; }
        [XmlElement("translation")]
        public Translation Translation { get; set; }
    }
}
