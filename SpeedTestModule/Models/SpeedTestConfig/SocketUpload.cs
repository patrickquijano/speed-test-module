﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("socket-upload")]
    public class SocketUpload
    {
        [XmlAttribute("testlength")]
        public int Testlength { get; set; }
        [XmlAttribute("initialthreads")]
        public string InitialThreads { get; set; }
        [XmlAttribute("minthreads")]
        public string MinThreads { get; set; }
        [XmlAttribute("maxthreads")]
        public int MaxThreads { get; set; }
        [XmlAttribute("threadratio")]
        public string ThreadRatio { get; set; }
        [XmlAttribute("maxsamplesize")]
        public int MaxSampleSize { get; set; }
        [XmlAttribute("minsamplesize")]
        public int MinSampleSize { get; set; }
        [XmlAttribute("startsamplesize")]
        public int StartSampleSize { get; set; }
        [XmlAttribute("startbuffersize")]
        public int StartBufferSize { get; set; }
        [XmlAttribute("bufferlength")]
        public int BufferLength { get; set; }
        [XmlAttribute("packetlength")]
        public int PacketLength { get; set; }
        [XmlAttribute("disabled")]
        public bool Disabled { get; set; }
    }
}
