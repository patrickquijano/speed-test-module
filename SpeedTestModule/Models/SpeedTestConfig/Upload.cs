﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("upload")]
    public class Upload
    {
        [XmlAttribute("testlength")]
        public long TestLength { get; set; }
        [XmlAttribute("ratio")]
        public long Ratio { get; set; }
        [XmlAttribute("initialtest")]
        public long InitialTest { get; set; }
        [XmlAttribute("mlongestsize")]
        public string MinTestSize { get; set; }
        [XmlAttribute("threads")]
        public long Threads { get; set; }
        [XmlAttribute("maxchunksize")]
        public string MaxChunkSize { get; set; }
        [XmlAttribute("maxchunkcount")]
        public long MaxChunkCount { get; set; }
        [XmlAttribute("threadsperurl")]
        public int ThreadsPerUrl { get; set; }
    }
}
