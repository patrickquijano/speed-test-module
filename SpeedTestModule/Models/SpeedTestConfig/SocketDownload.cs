﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("socket-download")]
    public class SocketDownload
    {
        [XmlAttribute("testlength")]
        public long TestLength { get; set; }
        [XmlAttribute("initialthreads")]
        public long InitialThreads { get; set; }
        [XmlAttribute("mlonghreads")]
        public long MinThreads { get; set; }
        [XmlAttribute("maxthreads")]
        public long MaxThreads { get; set; }
        [XmlAttribute("threadratio")]
        public string ThreadRatio { get; set; }
        [XmlAttribute("maxsamplesize")]
        public long MaxSampleSize { get; set; }
        [XmlAttribute("minsamplesize")]
        public long MinSampleSize { get; set; }
        [XmlAttribute("startsamplesize")]
        public long StartSampleSize { get; set; }
        [XmlAttribute("startbuffersize")]
        public long StartBufferSize { get; set; }
        [XmlAttribute("bufferlength")]
        public long BufferLength { get; set; }
        [XmlAttribute("packetlength")]
        public long PacketLength { get; set; }
        [XmlAttribute("readbuffer")]
        public long ReadBuffer { get; set; }
    }
}
