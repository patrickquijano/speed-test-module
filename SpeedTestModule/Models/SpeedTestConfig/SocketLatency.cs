﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("socket-latency")]
    public class SocketLatency
    {
        [XmlAttribute("testlength")]
        public int TestLength { get; set; }
        [XmlAttribute("waittime")]
        public int WaitTime { get; set; }
        [XmlAttribute("timeout")]
        public int Timeout { get; set; }
    }
}
