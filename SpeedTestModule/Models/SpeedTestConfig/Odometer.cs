﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("odometer")]
    public class Odometer
    {
        [XmlAttribute("start")]
        public long Start { get; set; }
        [XmlAttribute("rate")]
        public long Rate { get; set; }
    }
}
