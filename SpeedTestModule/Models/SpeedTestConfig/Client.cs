﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("client")]
    public class Client
    {
        [XmlAttribute("ip")]
        public string IPAddress { get; set; }
        [XmlAttribute("lat")]
        public double Latitude { get; set; }
        [XmlAttribute("lon")]
        public double Longitude { get; set; }
        [XmlAttribute("isp")]
        public string ISP { get; set; }
        [XmlAttribute("isprating")]
        public double ISPRating { get; set; }
        [XmlAttribute("rating")]
        public double Rating { get; set; }
        [XmlAttribute("ispdlavg")]
        public double ISPAverageDownload { get; set; }
        [XmlAttribute("ispulavg")]
        public double ISPAverageUpload { get; set; }
        [XmlAttribute("loggedin")]
        public long LoggedIn { get; set; }
        [XmlAttribute("country")]
        public string Country { get; set; }
        public GeoLocation Location => new GeoLocation
        {
            Latitude = this.Latitude,
            Longitude = this.Longitude,
        };
    }
}
