﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("times")]
    public class Times
    {
        [XmlAttribute("dl1")]
        public long Download1 { get; set; }
        [XmlAttribute("dl2")]
        public long Download2 { get; set; }
        [XmlAttribute("dl3")]
        public long Download3 { get; set; }
        [XmlAttribute("ul1")]
        public long Upload1 { get; set; }
        [XmlAttribute("ul2")]
        public long Upload2 { get; set; }
        [XmlAttribute("ul3")]
        public long Upload3 { get; set; }
    }
}
