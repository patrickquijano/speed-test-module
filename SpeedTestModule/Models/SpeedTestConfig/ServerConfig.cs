﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("server-config")]
    public class ServerConfig
    {
        [XmlAttribute("threadcount")]
        public long ThreadCount { get; set; }
        [XmlAttribute("ignoreids")]
        public string IgnoreIds { get; set; }
        [XmlAttribute("notonmap")]
        public string NotOnMap { get; set; }
        [XmlAttribute("forcepingid")]
        public string ForcePingId { get; set; }
        [XmlAttribute("preferredserverid")]
        public string PreferredServerId { get; set; }
    }
}
