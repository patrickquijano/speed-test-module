﻿using System.Xml.Serialization;

namespace SpeedTestModule.Models.SpeedTestConfig
{
    [XmlRoot("download")]
    public class Download
    {
        [XmlAttribute("testlength")]
        public long TestLength { get; set; }
        [XmlAttribute("initialtest")]
        public string InitialTest { get; set; }
        [XmlAttribute("mintestsize")]
        public string MinTestSize { get; set; }
        [XmlAttribute("threadsperurl")]
        public int ThreadsPerUrl { get; set; }
    }
}
