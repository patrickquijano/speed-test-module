﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SpeedTestModule.Extensions
{
    internal static class EnumerableExtension
    {
        public static void ForEach<T>(this IEnumerable<T> input, Action<T> callback)
        {
            if (input == null || !input.Any())
            {
                return;
            }
            foreach (var item in input)
            {
                callback(item);
            }
        }

        public static void ForEach<T>(this IEnumerable<T> input, Action<T, int> callback)
        {
            if (input == null || !input.Any())
            {
                return;
            }
            var i = 0;
            foreach (var item in input)
            {
                callback(item, i);
                i++;
            }
        }

        public static string StringJoin(this IEnumerable<string> input, string separator)
        {
            return input != null && input.Any() ? string.Join(separator, input) : string.Empty; 
        }
    }
}
