﻿using System;
using System.Text;
using System.Web;

namespace SpeedTestModule.Extensions
{
    internal static class UriExtension
    {
        public static Uri AddQuery(this Uri uri, string key, string value)
        {
            var httpNameValueCollection = HttpUtility.ParseQueryString(uri.Query);
            httpNameValueCollection.Remove(key);
            httpNameValueCollection.Add(key, value);
            var uribuilder = new UriBuilder(uri);
            var stringBuilder = new StringBuilder();
            if (httpNameValueCollection.Count == 0)
            {
                uribuilder.Query = string.Empty;
            }
            else
            {
                for (var i = 0; i < httpNameValueCollection.Count; i++)
                {
                    var queryKey = httpNameValueCollection.GetKey(i);
                    queryKey = HttpUtility.UrlEncode(queryKey);
                    queryKey = !queryKey.IsNullOrWhiteSpace() ? $"{queryKey}=" : string.Empty;
                    var queryValues = httpNameValueCollection.GetValues(i);
                    if (stringBuilder.Length > 0)
                    {
                        stringBuilder.Append('&');
                    }
                    if (queryValues == null || queryValues.Length == 0)
                    {
                        stringBuilder.Append(queryKey);
                    }
                    else
                    {
                        if (queryValues.Length == 1)
                        {
                            stringBuilder.Append(queryKey);
                            stringBuilder.Append(HttpUtility.UrlEncode(queryValues[0]));
                        }
                        else
                        {
                            for (var j = 0; j < queryValues.Length; j++)
                            {
                                if (j > 0)
                                {
                                    stringBuilder.Append('&');
                                }
                                stringBuilder.Append(queryKey);
                                stringBuilder.Append(HttpUtility.UrlEncode(queryValues[j]));
                            }
                        }
                    }
                }
                uribuilder.Query = stringBuilder.ToString();
            }

            return uribuilder.Uri;
        }
    }
}
