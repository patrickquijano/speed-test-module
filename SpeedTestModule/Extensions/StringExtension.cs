﻿namespace SpeedTestModule.Extensions
{
    internal static class StringExtension
    {
        public static bool IsNullOrWhiteSpace(this string input)
        {
            return string.IsNullOrWhiteSpace(input);
        }
    }
}
