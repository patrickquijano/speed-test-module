﻿using System;

namespace SpeedTestModule.IFactories
{
    public interface IUriFactory
    {
        Uri Create(string url, params string[] paths);
    }
}