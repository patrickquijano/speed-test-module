﻿using System.Net.Http;

namespace SpeedTestModule.IFactories
{
    public interface ISpeedTestHttpClientFactory
    {
        HttpClient Create();
    }
}