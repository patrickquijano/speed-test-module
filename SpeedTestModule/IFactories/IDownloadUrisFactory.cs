﻿using SpeedTestModule.Models.SpeedTestServers;
using System;
using System.Collections.Generic;

namespace SpeedTestModule.IFactories
{
    public interface IDownloadUrisFactory
    {
        IEnumerable<Uri> Create(Server server);
    }
}