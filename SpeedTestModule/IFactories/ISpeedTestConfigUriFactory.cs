﻿using System;

namespace SpeedTestModule.IFactories
{
    public interface ISpeedTestConfigUriFactory
    {
        Uri Create();
    }
}