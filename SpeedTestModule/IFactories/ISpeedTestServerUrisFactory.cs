﻿using System;
using System.Collections.Generic;

namespace SpeedTestModule.IFactories
{
    public interface ISpeedTestServerUrisFactory
    {
        IEnumerable<Uri> Create();
    }
}