﻿using SpeedTestModule.Models.SpeedTestConfig;
using System.Collections.Generic;

namespace SpeedTestModule.IFactories
{
    public interface IUploadStringsFactory
    {
        IEnumerable<string> Create(Upload upload);
    }
}