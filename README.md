# Speed Test Module

An Autofac module that performs speed test.

## Usage

Register the module in the Container.

```C#
var builder = new Autofac.ContainerBuilder();
builder.RegisterModule<SpeedTestModule.SpeedTestModule>();
```

Implement the service `SpeedTestModule.IServices.ICalculateSpeedTestService`.

```C#
using SpeedTestModule.IServices;
using System.Diagnostics;
// other imports
		
namespace SampleNamespace
{
	public class SampleClass
	{
		private readonly ICalculateSpeedTestService _CalculateSpeedTestService;
				
		public SampleClass(ICalculateSpeedTestService calculateSpeedTestService) {
			this._CalculateSpeedTestService = calculateSpeedTestService;
			// other implementations
		}
				
		public void SampleMethod() {
			var speedTest = this._CalculateSpeedTestService.CalculateAsync();
			Debug.WriteLine($"Download Speed: {speedTest.DownloadSpeedMbps:n2} Mbps");
			Debug.WriteLine($"Upload Speed: {speedTest.UploadSpeedMbps:n2} Mbps");
		}
	}
}
```
